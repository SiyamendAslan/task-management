var oComment = [];
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.task.controller.Task", {

		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Task").attachPatternMatched(this._onObjectMatched, this);
			var oProjectDataLine = new JSONModel({
				projectId: "",
				title: ""
			});
			this.getView().setModel(oProjectDataLine, "projectDataLine");
			var oTaskDataLine = new JSONModel({
				taskId: "",
				projectId: "",
				title: "",
				description: "",
				stateId: "",
				priorityId: "",
				typeId: ""
			});
			this.getView().setModel(oTaskDataLine, "taskDataLine");
		},
		onAfterRendering: function () {
			// standard code piece for mobile phone
			if (sap.ui.Device.system.phone) {
				this.getView().byId("detail").setShowNavButton(true);
				var oSplitCont = this.getView().byId("master"),
					ref = oSplitCont.getDomRef() && oSplitCont.getDomRef().parentNode;
				// set all parent elements to 100% height, this should be done by app developer, but just in case
				if (ref && !ref._sapUI5HeightFixed) {
					ref._sapUI5HeightFixed = true;
					while (ref && ref !== document.documentElement) {
						var $ref = jQuery(ref);
						if ($ref.attr("data-sap-ui-root-content")) { // Shell as parent does this already
							break;
						}
						if (!ref.style.height) {
							ref.style.height = "100%";
						}
						ref = ref.parentNode;
					}
				}

			}

		},

		onSelectionChange: function (oEvent) {
			//Find Selected project
			var projectId = oEvent.getParameter("listItem").getBindingContext("taskData").getProperty("projectId");
			// filter tile with projectId
			this._filterTile(projectId);
			if (sap.ui.Device.system.phone) {
				this.getView().byId("Split").toDetail(this.createId("detail"));
			}

		},
		onNewProject: function (oEvent) {
			this.getView().getModel("projectDataLine").setProperty("/projectId", "");
			this.getView().getModel("projectDataLine").setProperty("/title", "");
			this._getNewEditProjectDialog().open();
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").data("operation", "New");
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").setTitle(this.getView().getModel("i18n").getResourceBundle()
				.getText("NewProject"));
		},
		onEditProject: function (oEvent) {
			if (this.getView().byId("projectList").getSelectedItem() === null) {
				// this for after first render page and user not selected any item.
				this.getView().getModel("projectDataLine").setProperty("/projectId", this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("projectId"));
				this.getView().getModel("projectDataLine").setProperty("/title", this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("title"));
			} else {
				this.getView().getModel("projectDataLine").setProperty("/projectId", this.getView().byId("projectList").getSelectedItem().getBindingContext(
					"taskData").getProperty("projectId"));
				this.getView().getModel("projectDataLine").setProperty("/title", this.getView().byId("projectList").getSelectedItem().getBindingContext(
					"taskData").getProperty("title"));
			}
			this._getNewEditProjectDialog().open();
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").data("operation", "Edit");
			sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").setTitle(this.getView().getModel("i18n").getResourceBundle()
				.getText("EditProject"));

		},
		onDeleteProject: function (oEvent) {
			var selectedId;
			if (this.getView().byId("projectList").getSelectedItem() === null) {
				selectedId = this.getView().byId("projectList").getAggregation("items")[0]
					.getBindingContext("taskData").getProperty("projectId");
			} else {
				selectedId = this.getView().byId("projectList").getSelectedItem().
				getBindingContext("taskData").getProperty("projectId");
			}
			//Mapping Project
			var oProjectData = this.getView().getModel("taskData").getProperty("/Project");
			var oTaskData = this.getView().getModel("taskData").getProperty("/Task");
			var deletedIndex = oProjectData.map(function (e) {
				return e.projectId;
			}).indexOf(selectedId);
			oProjectData.splice(deletedIndex, 1);
			this.getView().getModel("taskData").setProperty("/Project", oProjectData);
			//Also we have delete dependcy Task 
			var filteredTask = oTaskData.filter(function (el) {
				return el.projectId !== selectedId;
			});
			// Delete related task
			oTaskData = filteredTask;
			this.getView().getModel("taskData").setProperty("/Task", oTaskData);
			this.getView().getModel("taskData").refresh(true);
			var projectId = this.getView().byId("projectList").getAggregation("items")[0]
				.getBindingContext("taskData").getProperty("projectId");
			this._filterTile(projectId);

		},
		onProjectDialogSave: function (oEvent) {
			if (this.getView().getModel("projectDataLine").getProperty("/title") === null ||
				this.getView().getModel("projectDataLine").getProperty("/title") === "") {
				sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
					"ProjectTitleError"));
			} else {
				var oProjectData = this.getView().getModel("taskData").getProperty("/Project");
				if (sap.ui.core.Fragment.byId("frgNewEditProject", "NewEditProjectDialog").data("operation") === "New") {
					// custom data equal New 
					var projectId = this.getView().getModel("taskData").getData().Project.length;
					oProjectData.push({
						projectId: parseInt(this.getView().getModel("taskData").getProperty("/Project/" + (projectId - 1) + "/projectId")) + 1,
						title: this.getView().getModel("projectDataLine").getProperty("/title")

					});

				} else {
					// Edit Project
					var Id = this.getView().getModel("projectDataLine").getProperty("/projectId");
					var editTask = oProjectData.findIndex(function (el) {
						return el.projectId === Id;
					});
					this.getView().getModel("taskData").setProperty("/Project/" + editTask + "/title",
						this.getView().getModel("projectDataLine").getProperty("/title"));
				}
				this._oNewEditProjectDialog.close();
				this.getView().getModel("taskData").refresh(true);
			}
		},
		onProjectDialogCancel: function (oEvent) {
			this._oNewEditProjectDialog.close();
		},
		onEditTask: function (oEvent) {
			oComment = [];
			this._getNewEditDialog().open();
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").data("operation", "Edit");
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").setTitle(this.getView().getModel("i18n").getResourceBundle().getText(
				"EditTask"));
			//	this._oNewEditDialog.setModel(this.getView().getModel("taskData"));
			var taskId = oEvent.getSource().getBindingContext("taskData").getProperty("taskId");
			var oTaskData = this.getView().getModel("taskData").getProperty("/Task");
			var editIndex = oTaskData.map(function (e) {
				return e.taskId;
			}).indexOf(taskId);
			this.getView().getModel("taskDataLine").setProperty("/", this.getView().getModel("taskData").getProperty("/Task/" + editIndex));

			var commentLength = oEvent.getSource().getBindingContext("taskData").getProperty("comment").length;
			//Task comment control for edit process.
			if (commentLength > 0) {
				for (var i = 0; i < commentLength; i++) {
					oComment.push({
						commentId: oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].commentId,
						title: oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].title
					});
					this.onAddComment(oEvent, oEvent.getSource().getBindingContext("taskData").getProperty("comment")[i].title);
				}
			}

		},
		onDelete: function (oEvent) {
			var deletedTask = oEvent.getSource().getBindingContext("taskData").getProperty("taskId");
			var filteredTask = this.getView().getModel("taskData").getData().Task.filter(function (el) {
				return el.taskId !== deletedTask;
			});
			this.getView().getModel("taskData").setProperty("/Task", filteredTask);
			this.getView().getModel("taskData").refresh(true);
		},
		onNewTask: function (oEvent) {
			// new Task Comment
			oComment = [];
			this.getView().getModel("taskDataLine").setProperty("/", {
				taskId: "",
				projectId: "",
				title: "",
				description: "",
				stateId: "",
				priorityId: "",
				typeId: ""
			});
			this._getNewEditDialog().open();
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").data("operation", "New");
			sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").setTitle(this.getView().getModel("i18n").getResourceBundle().getText(
				"NewTask"));
		},
		onProjectSearch: function (oEvent) {
			this.getView().byId("projectList").setBusy(true);
			var oList = this.getView().byId("projectList").getBinding("items");
			var sString = oEvent.getSource().getProperty("value");
			var filter = new sap.ui.model.Filter("title", sap.ui.model.FilterOperator.Contains, sString);
			oList.filter([filter]);
			this.getView().byId("projectList").setBusy(false);
		},
		onAddComment: function (oEvent, comment) {
			// if comment is undefined request come from new comment otherwise request come from json model
			var Id;
			var that = this;
			if (!comment) {

				comment = sap.ui.core.Fragment.byId("frgNewEdit", "comment").getValue();
				if (comment === null || comment === "") {
					sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText(
						"CommentError"));
				} else {
					// add comment JSON model if the comment new
					// comment id always get next number 
					if (sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").data("operation") === "New") {
						// new Task
						oComment.push({
							"commentId": oComment.length.toString(),
							"title": comment
						});
					} else {
						//Edit Task
						Id = this.getView().getModel("taskDataLine").getProperty("/taskId");
						Id = this.getView().getModel("taskData").getData().Task[Id].comment.length.toString();
						oComment.push({
							"commentId": Id,
							"title": comment
						});
					}
					sap.ui.core.Fragment.byId("frgNewEdit", "NewEdit").addFormElement(new sap.ui.layout.form.FormElement({
						label: "Comment",
						fields: [
							new sap.m.Text({
								text: comment
							}),
							new sap.m.Button({
								icon: "sap-icon://delete",
								type: "Reject",
								width: "20%",
								press: that.onCommentDelete
							})
						]
					}));
					sap.ui.core.Fragment.byId("frgNewEdit", "comment").setValue("");
				}
			} else {
				// Form element add in dialog for each comment
				sap.ui.core.Fragment.byId("frgNewEdit", "NewEdit").addFormElement(new sap.ui.layout.form.FormElement({
					label: "Comment",
					fields: [
						new sap.m.Text({
							text: comment
						}),
						new sap.m.Button({
							icon: "sap-icon://delete",
							type: "Reject",
							width: "20%",
							press: that.onCommentDelete
						})
					]
				}));
			}
		},
		onCommentDelete: function (oEvent) {
			var title = oEvent.getSource().getParent().getAggregation("fields")[0].getText();
			var filteredTask = oComment.filter(function (el) {
				return el.title !== title;
			});
			oComment = filteredTask;
			// delete in oComment
			oEvent.getSource().getParent().destroy();
		},
		onDialogCancel: function () {
			this._oNewEditDialog.close();
		},
		onDialogSave: function (oEvent) {
			// Title check
			if (this.getView().getModel("taskDataLine").getProperty("/title") === null ||
				this.getView().getModel("taskDataLine").getProperty("/title") === "") {
				sap.m.MessageToast.show(this.getView().getModel("i18n").getResourceBundle().getText("TaskTitleError"));
			} else {
				var oTaskData = this.getView().getModel("taskData").getProperty("/Task");
				var oTaskDataLine = this.getView().getModel("taskDataLine").getProperty("/");
				if (sap.ui.core.Fragment.byId("frgNewEdit", "NewEditDialog").data("operation") === "New") {
					// new Request
					var taskId = oTaskData.length;
					oTaskDataLine.taskId = parseInt(this.getView().getModel("taskData").getProperty("/Task/" + (taskId - 1) + "/taskId")) + 1;
					oTaskDataLine.comment = oComment;
					oTaskData.push(oTaskDataLine);
					this.getView().getModel("taskData").setProperty("/Task", oTaskData);
				} else {
					// Edit Request
					var Id = this.getView().getModel("taskDataLine").getProperty("/taskId");
					var editTask = oTaskData.findIndex(function (el) {
						return el.taskId === Id;
					});
					oTaskDataLine.comment = oComment;
					this.getView().getModel("taskData").setProperty("/Task/" + editTask, oTaskDataLine);
				}
				this._oNewEditDialog.close();
				this.getView().getModel("taskData").refresh(true);
			}
		},
		// this method for sap fiori client. in mobile master and detail are different views we have to do this.
		onPressDetailBack: function (oEvent) {
			this.getView().byId("Split").toMaster(this.createId("master"));
		},
		_filterTile: function (projectId) {
			var tile = this.getView().byId("TileContainerExpanded").getBinding("content");
			var tile1 = this.getView().byId("TileContainerExpanded2").getBinding("content");
			var tile2 = this.getView().byId("TileContainerExpanded3").getBinding("content");
			var filter = new sap.ui.model.Filter("projectId", sap.ui.model.FilterOperator.EQ, projectId);
			tile.filter([filter]);
			tile1.filter([filter]);
			tile2.filter([filter]);

		},
		_getNewEditDialog: function () {

			if (!this._oNewEditDialog) {
				this._oNewEditDialog = sap.ui.xmlfragment("frgNewEdit", "com.task.fragment.NewEdit", this);
				this.getView().addDependent(this._oNewEditDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditDialog);
			} else {
				// if the edit dialog open before we have to destroy it then call again.
				//Because I created Dynamic comment area.
				this._oNewEditDialog.destroy();
				this._oNewEditDialog = sap.ui.xmlfragment("frgNewEdit", "com.task.fragment.NewEdit", this);
				this.getView().addDependent(this._oNewEditDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditDialog);

			}

			return this._oNewEditDialog;

		},
		_getNewEditProjectDialog: function () {
			if (!this._oNewEditProjectDialog) {
				this._oNewEditProjectDialog = sap.ui.xmlfragment("frgNewEditProject", "com.task.fragment.NewEditProject", this);
				this.getView().addDependent(this._oNewEditProjectDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditProjectDialog);
			} else {
				// if the edit dialog open before we have to destroy it then call again.
				//Because I created Dynamic comment area.
				this._oNewEditProjectDialog.destroy();
				this._oNewEditProjectDialog = sap.ui.xmlfragment("frgNewEditProject", "com.task.fragment.NewEditProject", this);
				this.getView().addDependent(this._oNewEditProjectDialog);
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oNewEditProjectDialog);
			}
			this._oNewEditProjectDialog.setModel(this.getView().getModel("taskData"));
			return this._oNewEditProjectDialog;

		},
		_onObjectMatched: function (oEvent) {
			var userName = oEvent.getParameter("arguments").userName;
			this.getView().byId("master").setTitle(userName + '\'s Projects');
			// get the first item to list for filter
			var firstItem = this.getView().byId("projectList").getAggregation("items")[0]
				.getBindingContext("taskData").getProperty("projectId");
			var tile = this.getView().byId("TileContainerExpanded").getBinding("content");
			var filter = new sap.ui.model.Filter("projectId", sap.ui.model.FilterOperator.EQ, firstItem);
			tile.filter([filter]);
			oComment = [];
		}

	});
});