# Demo
Clone the repository and move into:

```console
git clone https://SiyamendAslan@bitbucket.org/SiyamendAslan/task-management.git
$ cd task-management
```
For Local Server

- Download 
[Python](https://www.python.org/ftp/python/3.6.5/python-3.6.5.exe) for server
- Install Python 

Run The Server
```console
$ python -m http.server 8000
```

Open your browser and go to [http://localhost:8000/](http://localhost:8000/)

For Web Demo
- If you have account in [hanatrial.ondemand.com](hanatrial.ondemand.com)
- Logon and go to [https://taskmanagement-p1942069864trial.dispatcher.hanatrial.ondemand.com/index.html](https://taskmanagement-p1942069864trial.dispatcher.hanatrial.ondemand.com/index.html)

# Task Management

Task Management is a simple and user friendly web app developed with SAPUI5.

Task Management project's goal is to allow the user

- To define the new projects also edit and delete projects.
- To define the new task also edit and delete task.
- In addition, to post comments,change the status and priority of the tasks.
